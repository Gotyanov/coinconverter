//
//  ExpandableViewController.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

protocol ExpandableViewController: class {
    var collapsedValue: CGFloat { get }
    var expandedValue: CGFloat { get }
    var currentValue: CGFloat { get }

    var runningAnimators: [(animator: UIViewPropertyAnimator, source: TransitionBehavior)] { get set }
    var currentState: ExpandableViewState { get set }
    var destinationState: ExpandableViewState? { get set }
    var transitionBehaviors: [TransitionBehavior] { get set }

    func getDuration(forMovingTo state: ExpandableViewState) -> TimeInterval
}

extension ExpandableViewController where Self: TransitionBehaviorDelegate {

    func visualFraction(relativeTo destinationState: ExpandableViewState) -> CGFloat {
        let fraction = (expandedValue - currentValue) / (expandedValue - collapsedValue)
        return destinationState == .collapsed ? fraction : 1 - fraction
    }

    func getDestinationState() -> ExpandableViewState {
        return destinationState ?? currentState.toggled()
    }

    func distanceBetweenStartAndFinish(relativeTo destinationState: ExpandableViewState) -> CGFloat {
        switch destinationState {
        case .collapsed:
            return collapsedValue - expandedValue
        case .expanded:
            return expandedValue - collapsedValue
        }
    }

    func addTransitionBehavior(_ behavior: TransitionBehavior) {
        behavior.delegate = self
        transitionBehaviors.append(behavior)
        behavior.activate()
    }

    func animateTransition() {
        willAnimateTransition()
        let destinationState = getDestinationState()
        let duration = getDuration(forMovingTo: destinationState)

        guard runningAnimators.isEmpty else { return }

        for behavior in transitionBehaviors {
            guard let animator = behavior.createAnimator(forMovingTo: destinationState, duration: duration)
                else { continue }

            animator.addCompletion { _ in
                dispatchPrecondition(condition: .onQueue(.main))
                if let index = self.runningAnimators.index(where: { $0.animator === animator }) {
                    self.runningAnimators.remove(at: index)
                } else {
                    print("can't find index")
                }

                if self.runningAnimators.isEmpty {
                    self.destinationState = nil
                    self.didAnimateTransition()
                }
            }

            runningAnimators.append((animator: animator, source: behavior))
            animator.startAnimation()
        }

        currentState = destinationState
        self.destinationState = destinationState
    }

    func pauseAnimations() {
        for animator in runningAnimators {
            animator.animator.pauseAnimation()
        }
    }

    func reverseAnimations() {
        for animator in runningAnimators {
            animator.animator.isReversed = !animator.animator.isReversed
        }
    }

    func startInteractiveTransition() {
        if runningAnimators.isEmpty {
            animateTransition()
        }

        pauseAnimations()
    }

    func updateInteractiveTransition(_ fraction: CGFloat) {
        for animator in runningAnimators {
            animator.animator.fractionComplete = fraction
        }
    }

    func continueInteractiveTransition() {
        let destinationState = getDestinationState()
        var overridenState: ExpandableViewState?

        for (animator, behavior) in runningAnimators {
            if let state = behavior.continueInteractiveTransition(animator: animator, movingToState: destinationState) {
                overridenState = state
            }
        }

        currentState = overridenState ?? destinationState
    }
}
