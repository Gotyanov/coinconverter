//
//  ExpandableViewState.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

enum ExpandableViewState {
    case expanded
    case collapsed
}

extension ExpandableViewState {
    func toggled() -> ExpandableViewState {
        switch self {
        case .expanded: return .collapsed
        case .collapsed: return .expanded
        }
    }
}
