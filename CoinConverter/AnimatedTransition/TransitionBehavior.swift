//
//  TransitionBehavior.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

protocol TransitionBehavior: class {
    var delegate: TransitionBehaviorDelegate? { get set }
    func activate()
    func createAnimator(forMovingTo state: ExpandableViewState, duration: TimeInterval) -> UIViewPropertyAnimator?
    func continueInteractiveTransition(animator: UIViewPropertyAnimator, movingToState state: ExpandableViewState) -> ExpandableViewState?
    func deactivate()
}

extension TransitionBehavior {
    func activate() { }
    func deactivate() { }
    func createAnimator(forMovingTo state: ExpandableViewState, duration: TimeInterval) -> UIViewPropertyAnimator? {
        return nil
    }

    func continueInteractiveTransition(animator: UIViewPropertyAnimator, movingToState state: ExpandableViewState) -> ExpandableViewState? {
        return nil
    }
}
