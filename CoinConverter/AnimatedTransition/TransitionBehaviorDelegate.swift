//
//  TransitionBehaviorDelegate.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

protocol TransitionBehaviorDelegate: class {
    var panVelocity: CGFloat { get set }
    func visualFraction(relativeTo destinationState: ExpandableViewState) -> CGFloat
    func getDestinationState() -> ExpandableViewState
    func distanceBetweenStartAndFinish(relativeTo destinationState: ExpandableViewState) -> CGFloat
    func animateTransition()
    func pauseAnimations()
    func reverseAnimations()
    func startInteractiveTransition()
    func updateInteractiveTransition(_ fraction: CGFloat)
    func continueInteractiveTransition()

    func willAnimateTransition()
    func didAnimateTransition()
}

extension TransitionBehaviorDelegate {
    func willAnimateTransition() { }
    func didAnimateTransition() { }
}
