//
//  DelegateTransitionBehavior.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 04.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

final class BlockTransitionBehavior: TransitionBehavior {
    weak var delegate: TransitionBehaviorDelegate?
    private let block: (ExpandableViewState) -> ()
    init(block: @escaping (ExpandableViewState) -> ()) {
        self.block = block
    }

    func createAnimator(forMovingTo state: ExpandableViewState, duration: TimeInterval) -> UIViewPropertyAnimator? {
        let animator = UIViewPropertyAnimator(duration: duration, curve: .easeOut) { [weak self] in
            self?.block(state)
        }

        return animator
    }

    func continueInteractiveTransition(animator: UIViewPropertyAnimator, movingToState state: ExpandableViewState) -> ExpandableViewState? {
        animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        return nil
    }
}
