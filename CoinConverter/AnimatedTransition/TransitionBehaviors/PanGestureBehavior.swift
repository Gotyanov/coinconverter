//
//  PanGestureBehavior.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

final class PanGestureBehavior: TransitionBehavior {
    weak var viewToTap: UIView?
    weak var delegate: TransitionBehaviorDelegate?
    weak var recognizer: UIPanGestureRecognizer?

    init(viewToPan: UIView) {
        self.viewToTap = viewToPan
    }

    func activate() {
        guard let viewToTap = viewToTap else { return }
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.recognizer = recognizer
        viewToTap.addGestureRecognizer(recognizer)
    }

    var interruptedFraction: CGFloat = 0
    var destinationState: ExpandableViewState = .collapsed

    @objc func handlePan(_ recognizer: UIPanGestureRecognizer) {
        guard let delegate = delegate else { return }
        let state = recognizer.state
        switch state {
        case .began:
            let destinationState = delegate.getDestinationState()
            self.destinationState = destinationState
            let visualFraction = delegate.visualFraction(relativeTo: destinationState)
            interruptedFraction = visualFraction
            delegate.startInteractiveTransition()
        case .changed:
            let translation = recognizer.translation(in: viewToTap).y
            let fraction = translation / delegate.distanceBetweenStartAndFinish(relativeTo: destinationState) + interruptedFraction
            delegate.updateInteractiveTransition(fraction)
            break
        case .ended:
            delegate.panVelocity = recognizer.velocity(in: viewToTap).y
            delegate.continueInteractiveTransition()
        default: break
        }
    }

    func deactivate() {
        guard let recognizer = recognizer, let viewToTap = viewToTap else { return }
        viewToTap.removeGestureRecognizer(recognizer)
    }
}
