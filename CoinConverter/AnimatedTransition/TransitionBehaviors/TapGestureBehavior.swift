//
//  TapGestureBehavior.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

final class TapGestureBehavior: TransitionBehavior {
    weak var viewToTap: UIView?
    weak var delegate: TransitionBehaviorDelegate?
    weak var recognizer: UITapGestureRecognizer?

    init(viewToTap: UIView) {
        self.viewToTap = viewToTap
    }

    func activate() {
        guard let viewToTap = viewToTap else { return }
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        self.recognizer = recognizer
        viewToTap.addGestureRecognizer(recognizer)
    }

    @objc func handleTap(_ recognizer: UITapGestureRecognizer) {
        guard recognizer.state == .ended else { return }
        delegate?.animateTransition()
    }

    func deactivate() {
        guard let recognizer = recognizer, let viewToTap = viewToTap else { return }
        self.viewToTap = nil
        self.recognizer = nil
        viewToTap.removeGestureRecognizer(recognizer)
    }
}
