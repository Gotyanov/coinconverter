//
//  ToggleConstraintsBehavior.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

final class ToggleConstraintsBehavior: TransitionBehavior {
    weak var delegate: TransitionBehaviorDelegate?

    private var constraintsForCollapsedState: [NSLayoutConstraint]
    private var constraintsForExpandedState: [NSLayoutConstraint]
    private var viewToLayout: UIView
    private var animationWasReversed = false

    init(constraintsForCollapsedState: [NSLayoutConstraint], constraintsForExpandedState: [NSLayoutConstraint], viewToLayout: UIView) {
        self.constraintsForCollapsedState = constraintsForCollapsedState
        self.constraintsForExpandedState = constraintsForExpandedState
        self.viewToLayout = viewToLayout
    }

    func createAnimator(forMovingTo state: ExpandableViewState, duration: TimeInterval) -> UIViewPropertyAnimator? {
        let animator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
            self.toggleConstraints(for: state)
            self.viewToLayout.layoutIfNeeded()
        }

        animator.addCompletion { [weak self] _ in
            guard let `self` = self else { return }

            // if animation was reversed — toggle constraints back
            if animator.isReversed {
                self.toggleConstraints(for: state.toggled())
                self.viewToLayout.setNeedsLayout()
            }
        }

        return animator
    }

    let speedThreshold: CGFloat = 500

    func continueInteractiveTransition(animator: UIViewPropertyAnimator, movingToState state: ExpandableViewState) -> ExpandableViewState? {
        guard let delegate = delegate else { return nil }
        let velocity = delegate.panVelocity
        let relativeVelocity = velocity / delegate.distanceBetweenStartAndFinish(relativeTo: state)
        let damping: CGFloat = state == .collapsed ? 1.0 : 0.8
        let timing = UISpringTimingParameters(dampingRatio: damping, initialVelocity: CGVector(dx: relativeVelocity, dy: 0))

        let fraction = delegate.visualFraction(relativeTo: state)

        var needReverse = false

        if fraction < 0.5 && state == .collapsed && velocity < speedThreshold {
            needReverse = true
        }

        if fraction < 0.5 && state == .expanded && velocity > -speedThreshold {
            needReverse = true
        }

        if needReverse {
            delegate.reverseAnimations()
        }
        animator.continueAnimation(withTimingParameters: timing, durationFactor: 0)

        if needReverse {
            return state.toggled()
        } else {
            return state
        }

    }

    public func toggleConstraints(for state: ExpandableViewState) {
        switch state {
        case .collapsed:
            NSLayoutConstraint.deactivate(constraintsForExpandedState)
            NSLayoutConstraint.activate(constraintsForCollapsedState)
        case .expanded:
            NSLayoutConstraint.deactivate(constraintsForCollapsedState)
            NSLayoutConstraint.activate(constraintsForExpandedState)
        }
    }
}
