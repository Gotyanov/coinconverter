//
//  CoinImageLoader.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation
import RxSwift
import UIKit

final class CoinImageLoader {

    private let basePath = "https://s2.coinmarketcap.com/static/img/coins/128x128/"

    private lazy var session: URLSession = {
        let config = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: config)
        return session
    }()

    func loadImage(for coinID: Int) -> Observable<UIImage?> {
        let url = URL(string: basePath + "\(coinID).png")!
        return Observable.create { [session] observer in
            let request = URLRequest(url: url)
            let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
                guard error == nil else {
                    observer.onError(RequestError.transportError(error!))
                    return
                }

                guard let response = response as? HTTPURLResponse else {
                    fatalError("response should be HTTPURLResponse")
                }

                guard case 200..<300 = response.statusCode else {
                    observer.onError(RequestError.notSuccessfulStatusCode(response))
                    return
                }

                guard let data = data else {
                    observer.onNext(nil)
                    observer.onCompleted()
                    return
                }

                let image = UIImage(data: data)
                observer.onNext(image)
                observer.onCompleted()
            })

            task.priority = URLSessionDataTask.lowPriority
            task.resume()

            return Disposables.create {
                task.cancel()
            }
        }
    }
}

extension CoinImageLoader: CoinImageSource {
    func getImage(for coin: CurrencyType) -> Observable<UIImage?> {
        guard let id = coin.currencyID else {
            return Observable.just(nil)
        }
        
        return loadImage(for: id)
    }
}
