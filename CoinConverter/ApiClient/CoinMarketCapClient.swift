//
//  CoinMarketCapClient.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation
import RxSwift

enum RequestError: Error {
    case transportError(Error)
    case notSuccessfulStatusCode(HTTPURLResponse)
    case serviceError(String)
}

final class CoinMarketCapClient {

    private let apiBaseURL = URL(string: "https://api.coinmarketcap.com/v2")!

    func getListings() -> Observable<ListingModel> {
        return makeRequest(url: apiBaseURL.appendingPathComponent("/listings"))
    }

    func getTicker(for coinID: Int, convertTo targetCoin: String) -> Observable<TickerModel> {
        let pathUrl = apiBaseURL.appendingPathComponent("/ticker/\(coinID)/")
        guard var urlComponents = URLComponents(url: pathUrl, resolvingAgainstBaseURL: false) else {
            fatalError("Unable to create urlComponents")
        }

        urlComponents.queryItems = [URLQueryItem(name: "convert", value: targetCoin)]

        guard let url = urlComponents.url else {
            fatalError("Can not create url")
        }

        return makeRequest(url: url)
    }

    private func makeRequest<T: Decodable>(url: URL) -> Observable<T> {
        return Observable<T>.create { observer in
            let request = URLRequest(url: url)
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) in
                guard error == nil else {
                    observer.onError(RequestError.transportError(error!))
                    return
                }

                guard let response = response as? HTTPURLResponse else {
                    fatalError("response should be HTTPURLResponse")
                }

                guard case 200..<300 = response.statusCode else {
                    observer.onError(RequestError.notSuccessfulStatusCode(response))
                    return
                }

                do {
                    let model: T = try JSONDecoder().decode(T.self, from: data ?? Data())
                    observer.onNext(model)
                    observer.onCompleted()
                    return
                } catch let error {
                    observer.onError(error)
                }
            }

            task.resume()

            return Disposables.create {
                task.cancel()
            }
        }
    }
}

final class ListingModel: Codable {
    struct ListingModelCoin: Codable {
        let id: Int
        let name: String
        let symbol: String
    }

    struct ListingModelMetadata: Codable {
        let timestamp: Int
        let num_cryptocurrencies: Int
        let error: String?
    }

    let data: [ListingModelCoin]
    let metadata: ListingModelMetadata

    init(data: [ListingModelCoin], metadata: ListingModelMetadata) {
        self.data = data
        self.metadata = metadata
    }
}

final class TickerModel: Codable {
    struct TickerModelQuote: Codable {
        let price: Decimal
    }

    struct TickerModelInfo: Codable {
        let id: Int
        let name: String
        let symbol: String
        let rank: Int
        let quotes: [String: TickerModelQuote]
        let last_updated: Int
    }

    struct TickerModelMetadata: Codable {
        let timestamp: Int
        let error: String?
    }

    let data: TickerModelInfo
    let metadata: TickerModelMetadata
}
