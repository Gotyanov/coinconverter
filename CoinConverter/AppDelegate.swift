//
//  AppDelegate.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 31.05.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let appSettings = AppSettings()
    var imageSource: CoinImageSource?
    var priceProvider: PriceProvider?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        let settings = appSettings
        let rootViewController = self.window!.rootViewController! as! CalculationViewController
        createPersistentContainer { (persistentContainer) in
            let viewContext = persistentContainer.viewContext

            if !settings.dbWasPrefilled {
                fillDb(objectContext: viewContext, settings: settings)
            }

            let compoundImageSource = CompoundCoinImageSource(persistentStore: viewContext,
                                                              remoteSource: CoinImageLoader(),
                                                              assetsSource: AssetImageSource())

            self.imageSource = compoundImageSource

            rootViewController.viewContext = viewContext
            rootViewController.imageSource = compoundImageSource
        }

        let priceProvider = CachedPriceProvider(sourcePriceProvider: CoinMarketCapClient())
        self.priceProvider = priceProvider

        rootViewController.priceProvider = priceProvider
        rootViewController.sourceAndTargetCurrencySetting = settings

        return true
    }
}

