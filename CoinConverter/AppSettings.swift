//
//  AppSettings.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

final class AppSettings {
    private struct Keys {
        static let dbWasPrefilled = "dbWasPrefilled"
        static let sourceCurrency = "sourceCurrency"
        static let targetCurrency = "targetCurrency"
    }

    private var userDefaults: UserDefaults {
        return UserDefaults.standard
    }
}

protocol DbWasPrefilledSetting: class {
    var dbWasPrefilled: Bool { get set }
}

protocol SourceAndTargetCurrencySetting {
    var sourceCurrency: CurrencyType { get set }
    var targetCurrency: CurrencyType { get set }
}

extension AppSettings: DbWasPrefilledSetting {
    var dbWasPrefilled: Bool {
        get {
            return userDefaults.bool(forKey: Keys.dbWasPrefilled)
        }
        set {
            userDefaults.set(newValue, forKey: Keys.dbWasPrefilled)
        }
    }
}

extension AppSettings: SourceAndTargetCurrencySetting {
    var defaultSourceCurrency: CurrencyType {
        return ThreadSafeCoin(id: 1, name: "Bitcoin", symbol: "BTC")
    }

    var defaultTargetCurrency: CurrencyType {
        return ThreadSafeCoin(id: nil, name: "US Dollar", symbol: "USD")
    }

    private func readCurrency(forKey key: String) -> ThreadSafeCoin? {
        guard let data = userDefaults.object(forKey: key) as? Data else { return nil }

        let decoder = JSONDecoder()
        let coin = try? decoder.decode(ThreadSafeCoin.self, from: data)
        return coin
    }

    private func writeCurrency(_ value: ThreadSafeCoin, forKey key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(value) {
            userDefaults.set(encoded, forKey: key)
        }
    }

    var sourceCurrency: CurrencyType {
        get {
            return readCurrency(forKey: Keys.sourceCurrency) ?? defaultSourceCurrency
        }
        set {
            writeCurrency(ThreadSafeCoin(currency: newValue), forKey: Keys.sourceCurrency)
        }
    }

    var targetCurrency: CurrencyType {
        get {
            return readCurrency(forKey: Keys.targetCurrency) ?? defaultTargetCurrency
        }
        set {
            writeCurrency(ThreadSafeCoin(currency: newValue), forKey: Keys.targetCurrency)
        }
    }
}

