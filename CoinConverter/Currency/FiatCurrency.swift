//
//  Currency.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 31.05.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

struct FiatCurrency: Codable {
    let code: String
    let name: String
    let symbol: String
}

extension FiatCurrency: CurrencyType {
    var currencyID: Int? { return nil }
    var currencyName: String { return name }
    var currencySymbol: String { return code }
}
