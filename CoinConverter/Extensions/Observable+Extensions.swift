//
//  Observable+Extensions.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

import RxSwift

private let USER_DELAY_INPUT = 0.3

extension Observable where Element == String {
    var throttledUserInput: Observable<Element> {
        return throttle(USER_DELAY_INPUT, scheduler: MainScheduler.instance).distinctUntilChanged()
    }
}
