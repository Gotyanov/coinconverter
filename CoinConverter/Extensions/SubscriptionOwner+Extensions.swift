//
//  SubscriptionOwner+Extensions.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

extension SubscriptionOwner {
    func adjustContentInsets(for scrollView: UIScrollView) {
        let notificationCenter = NotificationCenter.default

        let didShowSubscription = notificationCenter.addObserver(forName: NSNotification.Name.UIKeyboardDidShow, object: nil, queue: nil) { [weak scrollView] notification in

            guard let scrollView = scrollView else { return }

            let keyboardFrame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! CGRect

            let viewFrame = scrollView.convert(scrollView.bounds, to: nil)
            let tableViewBottom = viewFrame.maxY
            let bottomInset = max(0, tableViewBottom - keyboardFrame.origin.y - scrollView.safeAreaInsets.bottom)
            scrollView.contentInset.bottom = bottomInset
            scrollView.scrollIndicatorInsets.bottom = bottomInset
        }


        let willHideSubscription = notificationCenter.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { [weak scrollView] notification in
            guard let scrollView = scrollView else { return }

            scrollView.contentInset.bottom = 0
            scrollView.scrollIndicatorInsets.bottom = 0
        }

        self.addSubscription(withName: "keyboardNotificationSubscriptions", [didShowSubscription, willHideSubscription])
    }
}
