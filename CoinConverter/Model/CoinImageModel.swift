//
//  CoinImageModel.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import CoreData

@objc(CoinImageModel)
final class CoinImageModel: NSManagedObject {
    @NSManaged var symbol: String
    @NSManaged var imageData: Data

    static func insert(into context: NSManagedObjectContext, symbol: String, imageData: Data) {
        let model: CoinImageModel = context.insertNewObject()
        model.symbol = symbol
        model.imageData = imageData
    }
}

extension CoinImageModel: ManagedObject {

}
