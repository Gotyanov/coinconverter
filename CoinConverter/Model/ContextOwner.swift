//
//  ContextOwner.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import CoreData

final class ContextOwner {
    let viewContext: NSManagedObjectContext
    let syncContext: NSManagedObjectContext

    init(viewContext: NSManagedObjectContext, syncContext: NSManagedObjectContext) {
        self.viewContext = viewContext
        self.syncContext = syncContext
    }
}
