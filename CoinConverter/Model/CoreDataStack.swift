//
//  CoreDataStack.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import CoreData

fileprivate let containerName = "CoinConverter"

func createPersistentContainer(_ completion: @escaping (NSPersistentContainer) -> ()) {
    let container = NSPersistentContainer(name: containerName)
    container.loadPersistentStores { (_, error) in
        guard error == nil else {
            fatalError("Failed to load store: \(error!)")
        }

        DispatchQueue.main.async {
            completion(container)
        }
    }
}
