//
//  CurrencyDataSource.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

protocol CurrencyDataSource {
    associatedtype Currency: CurrencyType

    func getCurrencyValues() -> [Currency]
}

struct InstalledListingFromCoinMarket: CurrencyDataSource {
    func getCurrencyValues() -> [ListingModel.ListingModelCoin] {
        guard let listingLocalUrl = Bundle.main.url(forResource: "CoinListings", withExtension: "json"),
            let data = try? Data(contentsOf: listingLocalUrl)
            else { fatalError("can't get CoinListings.json") }

        let model = try! JSONDecoder().decode(ListingModel.self, from: data)
        return model.data
    }
}

struct InstalledFiatCurrencyValues: CurrencyDataSource {
    func getCurrencyValues() -> [FiatCurrency] {
        guard let url = Bundle.main.url(forResource: "CurrencyList", withExtension: "plist"),
            let data = try? Data(contentsOf: url)
            else { fatalError("can't get CurrencyList.plist") }

        let decoder = PropertyListDecoder()
        let currencyInfos: [FiatCurrency] = try! decoder.decode([FiatCurrency].self, from: data)
        return currencyInfos
    }
}
