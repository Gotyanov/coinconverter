//
//  CurrencyModel.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import CoreData

@objc(CurrencyModel)
final class CurrencyModel: NSManagedObject {
    @NSManaged public var id: Int32
    @NSManaged public var name: String
    @NSManaged public var nameUppercased: String
    @NSManaged public var symbol: String
    @NSManaged public var isFavorite: Bool

    public static func insert(into context: NSManagedObjectContext, id: Int32?, name: String, symbol: String, isFavorite: Bool) -> CurrencyModel {
        let model: CurrencyModel = context.insertNewObject()
        model.id = id ?? 0
        model.name = name
        model.nameUppercased = name.uppercased()
        model.symbol = symbol
        model.isFavorite = isFavorite
        return model
    }

    public static func insert<Currency: CurrencyType>(into context: NSManagedObjectContext, currency: Currency) -> CurrencyModel {
        return insert(into: context,
                      id: currency.currencyID.flatMap(Int32.init),
                      name: currency.currencyName,
                      symbol: currency.currencySymbol,
                      isFavorite: false)
    }
}

extension CurrencyModel: ManagedObject {
    static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(keyPath: \CurrencyModel.name, ascending: true)]
    }
}

extension CurrencyModel {
    static func searchPredicate(query: String) -> NSPredicate {
        guard query != "" else {
            return defaultPredicate
        }

        let queryUppercased = query.uppercased()
        let searchInNamePredicate = NSPredicate(format: "%K BEGINSWITH[n] %@", #keyPath(CurrencyModel.nameUppercased), queryUppercased)
        let searchInSymbolPredicate = NSPredicate(format: "%K BEGINSWITH[n] %@", #keyPath(CurrencyModel.symbol), queryUppercased)
        let searchPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [searchInNamePredicate, searchInSymbolPredicate])
        return searchPredicate
    }

    static func cryptoCurrencyPredicate() -> NSPredicate {
        return NSPredicate(format: "%K != %@", #keyPath(CurrencyModel.id), NSNumber(value: 0))
    }

    static func fiatCurrencyPredicate() -> NSPredicate {
        return NSPredicate(format: "%K == %@", #keyPath(CurrencyModel.id), NSNumber(value: 0))
    }

    static func starredCurrencyPredicate() -> NSPredicate {
        return NSPredicate(format: "%K == TRUE", #keyPath(CurrencyModel.isFavorite))
    }
}

extension CurrencyModel: CurrencyType {
    var currencyID: Int? {
        return id == 0 ? nil : Int(id)
    }

    var currencyName: String {
        return name
    }

    var currencySymbol: String {
        return symbol
    }
}
