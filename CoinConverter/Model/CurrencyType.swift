//
//  CurrencyType.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

protocol CurrencyType {
    var currencyID: Int? { get }
    var currencyName: String { get }
    var currencySymbol: String { get }
}

extension ListingModel.ListingModelCoin: CurrencyType {
    var currencyID: Int? { return id }
    var currencyName: String { return name }
    var currencySymbol: String { return symbol }
}
