//
//  trash.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import CoreData

func fillDb(objectContext: NSManagedObjectContext, settings: DbWasPrefilledSetting) {
    let dbWasPrefilled = settings.dbWasPrefilled
    guard !dbWasPrefilled else { return }

    let fiatDataSource = InstalledFiatCurrencyValues()
    let fiatCurrencyValues = fiatDataSource.getCurrencyValues()
    for currency in fiatCurrencyValues {
        _ = CurrencyModel.insert(into: objectContext, currency: currency)
    }

    let listingDataSource = InstalledListingFromCoinMarket()
    let listingCurrencyValues = listingDataSource.getCurrencyValues()
    for currency in listingCurrencyValues {
        _ = CurrencyModel.insert(into: objectContext, currency: currency)
    }

    let successSave = objectContext.saveOrRollback()
    if successSave {
        settings.dbWasPrefilled = true
    }
}

