//
//  AssetImageSource.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

struct AssetImageSource {
    func getImage(currencySymbol: String) -> UIImage? {
        return UIImage(named: currencySymbol)
    }
}

extension AssetImageSource: CoinImageSource {
    func getImage(for coin: CurrencyType) -> Observable<UIImage?> {
        let image = getImage(currencySymbol: coin.currencySymbol)
        return Observable.just(image)
    }
}
