//
//  CompoundCoinImageSource.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

final class CompoundCoinImageSource: CoinImageSource {

    private enum Source {
        case persistent, assets, remote
    }

    private let imageCache = ImageCacheManager()
    private let persistentStore: CoinImageWritableSource
    private let remoteSource: CoinImageSource
    private let assetsSource: CoinImageSource

    init(persistentStore: CoinImageWritableSource, remoteSource: CoinImageSource, assetsSource: CoinImageSource) {
        self.persistentStore = persistentStore
        self.remoteSource = remoteSource
        self.assetsSource = assetsSource
    }

    func getImage(for coin: CurrencyType) -> Observable<UIImage?> {
        let coin = ThreadSafeCoin(currency: coin)

        if let cachedImage = imageCache.getImage(for: coin.currencySymbol) {
            return Observable.just(cachedImage)
        }

        let observable: Observable<(image: UIImage?, source: Source)> = Observable.concat([
            assetsSource.getImage(for: coin).map { (image: $0, source: Source.assets) },
            persistentStore.getImage(for: coin).map { (image: $0, source: Source.persistent) },
            remoteSource.getImage(for: coin).map { (image: $0, source: Source.remote) }
            ])

        let nonNilImageObservable = observable.filter({ $0.image != nil })

        let storeImageSideEffectObservable = nonNilImageObservable.do(onNext: { [weak self] (image, source) in
            guard let `self` = self, let image = image else { return }

            if source == .persistent || source == .remote {
                self.imageCache.setImage(image, for: coin.currencySymbol)
            }

            if source == .remote {
                self.persistentStore.saveImage(image: image, for: coin)
            }
        })

        return storeImageSideEffectObservable.take(1).map({ $0.image }).ifEmpty(default: nil)
    }
}

