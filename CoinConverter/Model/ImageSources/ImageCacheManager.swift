//
//  ImageCacheManager.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

final class ImageCacheManager {
    let cache = { () -> NSCache<NSString, UIImage> in
        let cache = NSCache<NSString, UIImage>()
        cache.name = "ImageCacheManager"
        return cache
    }()

    func getImage(for key: String) -> UIImage? {
        return cache.object(forKey: key as NSString)
    }

    func setImage(_ value: UIImage, for key: String) {
        cache.setObject(value, forKey: key as NSString)
    }
}
