//
//  ManagedObjectContext.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import CoreData
import RxSwift
import UIKit

extension NSManagedObjectContext: CoinImageWritableSource {
    func getImage(for coin: CurrencyType) -> Observable<UIImage?> {
        let symbol = coin.currencySymbol
        return Observable.create { observer in
            self.perform {
                let predicate = NSPredicate(format: "%K == %@", #keyPath(CoinImageModel.symbol), symbol)
                CoinImageModel.findOrFetchAsync(in: self, matching: predicate, completionBlock: { (coinImageModel, error) in
                    guard error == nil else {
                        observer.onError(error!)
                        return
                    }

                    let data = coinImageModel?.imageData
                    let image = data.flatMap { UIImage(data: $0) }
                    observer.onNext(image)
                    observer.onCompleted()
                })
            }

            return Disposables.create()
        }
    }

    func saveImage(image: UIImage, for coin: CurrencyType) {
        let symbol = coin.currencySymbol
        guard let data = UIImagePNGRepresentation(image) else { return }

        performChanges {
            CoinImageModel.insert(into: self, symbol: symbol, imageData: data)
        }
    }
}
