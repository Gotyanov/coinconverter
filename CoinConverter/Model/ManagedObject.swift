//
//  ManagedObject.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import CoreData

protocol ManagedObject: class, NSFetchRequestResult {
    static var entityName: String { get }
    static var defaultSortDescriptors: [NSSortDescriptor] { get }
    static var defaultPredicate: NSPredicate { get }
}

extension ManagedObject {
    static var defaultSortDescriptors: [NSSortDescriptor] { return [] }
    static var defaultPredicate: NSPredicate { return NSPredicate(value: true) }

    static var sortedFetchRequest: NSFetchRequest<Self> {
        let fetchRequest = NSFetchRequest<Self>(entityName: entityName)
        fetchRequest.sortDescriptors = defaultSortDescriptors
        fetchRequest.predicate = defaultPredicate
        return fetchRequest
    }
}

extension ManagedObject where Self: NSManagedObject {
    static var entityName: String {
        return entity().name!
    }

    static func materializedObject(in context: NSManagedObjectContext, matching predicate: NSPredicate) -> Self? {
        for object in context.registeredObjects where !object.isFault {
            guard let result = object as? Self, predicate.evaluate(with: result) else { continue }
            return result
        }

        return nil
    }

    static func findOrFetchAsync(in context: NSManagedObjectContext, matching predicate: NSPredicate, completionBlock: @escaping (Self?, Error?) -> ()) {
        if let object = materializedObject(in: context, matching: predicate) {
            completionBlock(object, nil)
            return
        }
        
        fetchAsync(in: context, configure: { (request) in
            request.fetchLimit = 1
            request.returnsObjectsAsFaults = false
            request.predicate = predicate
        }) { (objects, error) in
            completionBlock(objects?.first, error)
        }
    }

    static func fetchAsync(in context: NSManagedObjectContext, configure: ((NSFetchRequest<Self>) -> ()) = { _ in }, completionBlock: @escaping ([Self]?, Error?) -> ()) {
        let fetchRequest = NSFetchRequest<Self>(entityName: entityName)
        configure(fetchRequest)
        let asyncFetchRequest = NSAsynchronousFetchRequest(fetchRequest: fetchRequest) { result in
            completionBlock(result.finalResult, result.operationError)
        }

        do {
            try context.execute(asyncFetchRequest)
        } catch {
            completionBlock(nil, error)
        }
    }
}


