//
//  CachedPriceProvider.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 04.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation
import RxSwift

final class CachedPriceProvider: PriceProvider {

    let cacheInvalidationTimeInterval: TimeInterval = 60

    private let cache: NSCache<NSString, CachedPrice>

    let sourcePriceProvider: PriceProvider

    init(sourcePriceProvider: PriceProvider) {
        self.sourcePriceProvider = sourcePriceProvider
        cache = NSCache<NSString, CachedPrice>()
    }

    func getPrice(for currency: CurrencyType, baseCurrency: CurrencyType) -> Observable<PriceProviderResult> {
        if let cachedResult = getResultFromCache(currency.currencySymbol, baseCurrency.currencySymbol) {
            return .just(cachedResult)
        }

        let key = "\(currency.currencySymbol):\(baseCurrency.currencySymbol)"
        return sourcePriceProvider.getPrice(for: currency, baseCurrency: baseCurrency)
            .do(onNext: { result in
                let cachedPrice = CachedPrice(result: result, timestamp: self.now)
                self.cache.setObject(cachedPrice, forKey: key as NSString)
            })
    }

    private var now: TimeInterval {
        return Date().timeIntervalSinceReferenceDate
    }

    private func getResultFromCache(_ symbol1: String, _ symbol2: String) -> PriceProviderResult? {
        let key = "\(symbol1):\(symbol2)"
        let reversedKey = "\(symbol2):\(symbol1)"

        var isReversed = false

        var maybeResult = cache.object(forKey: key as NSString)
        if maybeResult == nil {
            isReversed = true
            maybeResult = cache.object(forKey: reversedKey as NSString)
        }

        guard let result = maybeResult else { return nil }
        guard result.timestamp + cacheInvalidationTimeInterval > now else {
            cache.removeObject(forKey: (isReversed ? reversedKey : key) as NSString)
            return nil
        }

        switch result.result {
        case .price(let price):
            return isReversed ? .price(1 / price) : .price(price)
        case .notSupportedConversion:
            return .notSupportedConversion
        }
    }
}

fileprivate final class CachedPrice {
    let result: PriceProviderResult
    let timestamp: TimeInterval

    init(result: PriceProviderResult, timestamp: TimeInterval) {
        self.result = result
        self.timestamp = timestamp
    }
}
