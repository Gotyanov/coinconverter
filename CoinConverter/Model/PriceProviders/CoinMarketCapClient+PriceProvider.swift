//
//  CoinMarketCapClient+PriceProvider.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 04.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation
import RxSwift

extension CoinMarketCapClient: PriceProvider {
    func getPrice(for currency: CurrencyType, baseCurrency: CurrencyType) -> Observable<PriceProviderResult> {
        guard currency.currencySymbol != baseCurrency.currencySymbol else {
            return Observable.just(.price(1))
        }

        guard currency.currencyID != nil || baseCurrency.currencyID != nil else {
            return Observable.just(.notSupportedConversion)
        }

        guard let id = currency.currencyID else {
            return getPrice(for: baseCurrency, baseCurrency: currency).map { result -> PriceProviderResult in
                switch result {
                case .notSupportedConversion: return .notSupportedConversion
                case .price(let price): return .price(1.0 / price)
                }
            }
        }

        let baseCurrencySymbol = baseCurrency.currencySymbol
        let tickerObservable = getTicker(for: id, convertTo: baseCurrency.currencySymbol)

        return tickerObservable.map { ticker throws -> PriceProviderResult in
            let error = ticker.metadata.error
            guard error == nil else {
                throw NSError(domain: error!, code: 1, userInfo: ["id": id, "toCurrency": baseCurrencySymbol])
            }

            guard let quote = ticker.data.quotes[baseCurrencySymbol] else {
                throw NSError(domain: "ticker does not contain specific quote", code: 2, userInfo: ["id": id, "toCurrency": baseCurrencySymbol])
            }

            return PriceProviderResult.price(Double(truncating: quote.price as NSNumber))
        }
    }
}
