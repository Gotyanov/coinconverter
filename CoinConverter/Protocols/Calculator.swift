//
//  Calculator.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation
import RxSwift

enum CalculatorResult {
    case formattedAmount(String)
    case notSupportedConversion
    case error(Error)
    case inProgress
}

protocol Calculator {
    var priceProvider: PriceProvider { get }

    var sourceCurrency: CurrencyType { get }
    var targetCurrency: CurrencyType { get }

    func inputAmount(_ value: String)

    func updateSource(formattedAmount: String)
    func updateTarget(_ result: CalculatorResult)
}

extension Calculator where Self: SubscriptionOwner {
    func inputAmount(_ value: String) {
        guard let parsedValue = parseAmount(value) else {
            return
        }

        let parsedValueDouble = Double(truncating: parsedValue as NSNumber)

        let formattedAmountForSource = formatAmountForSource(parsedValue, originalString: value)

        guard parsedValue != 0 else {
            updateSource(formattedAmount: formattedAmountForSource)
            updateTarget(.formattedAmount(formatAmount(0)))
            return
        }

        updateSource(formattedAmount: formattedAmountForSource)
        updateTarget(.inProgress)

        let disposeBag = DisposeBag()

        priceProvider.getPrice(for: sourceCurrency, baseCurrency: targetCurrency)
            .subscribe(onNext: { result in
                switch result {
                case .notSupportedConversion:
                    DispatchQueue.main.async {
                        self.updateTarget(.notSupportedConversion)
                    }
                case .price(let price):
                    let calculatedValue = price * parsedValueDouble
                    let formattedCalculatedValue = formatAmount(calculatedValue)
                    DispatchQueue.main.async {
                        self.updateTarget(.formattedAmount(formattedCalculatedValue))
                    }
                }

            }, onError: { error in
                DispatchQueue.main.async {
                    self.updateTarget(.error(error))
                }
            }).disposed(by: disposeBag)

        addSubscription(withName: "calculation", disposeBag)
    }
}

fileprivate func parseAmount(_ string: String) -> Decimal? {
    let groupingSeparator = FormatterStore.formatter.currencyGroupingSeparator ?? ""
    var string = string
    if groupingSeparator != "" {
        string = string.replacingOccurrences(of: groupingSeparator, with: "")
    }

    return FormatterStore.formatter.number(from: string)?.decimalValue
}

fileprivate func formatAmount(_ value: Double) -> String {
    return (FormatterStore.formatter.string(from: value as NSNumber) ?? "")
        .trimmingCharacters(in: .whitespaces)
}

fileprivate func formatAmountForSource(_ value: Decimal, originalString: String) -> String {
    let formatter = FormatterStore.formatterForSource

    let regex = try! NSRegularExpression(pattern: NSRegularExpression.escapedPattern(for: formatter.decimalSeparator) + "0+$", options: [])
    let range = NSRange.init(location: 0, length: (originalString as NSString).length)
    if let match = regex.firstMatch(in: originalString, options: [], range: range) {
        formatter.minimumFractionDigits = match.range.length - 1
        formatter.minimumSignificantDigits = match.range.length
    } else {
        formatter.minimumFractionDigits = 0
        formatter.minimumSignificantDigits = 0
    }

    var formattedString = (formatter.string(from: value as NSNumber) ?? "").trimmingCharacters(in: CharacterSet.whitespaces)

    if originalString.last?.description == formatter.decimalSeparator {
        formattedString += formatter.decimalSeparator
    }

    return formattedString
}

fileprivate struct FormatterStore {
    static let formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        formatter.maximumSignificantDigits = 6
        return formatter
    }()

    static let formatterForSource: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        formatter.minimumSignificantDigits = 0
        formatter.maximumSignificantDigits = 100
        return formatter
    }()
}
