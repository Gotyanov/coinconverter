//
//  CoinImageDataSource.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

protocol CoinImageSource {
    func getImage(for coin: CurrencyType) -> Observable<UIImage?>
}

protocol CoinImageWritableSource: CoinImageSource {
    func saveImage(image: UIImage, for coin: CurrencyType)
}
