//
//  CoinView.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift
import CoreData

protocol CoinView {
    func configure(name: String, symbol: String, isStarred: Bool, imageObservable: Observable<UIImage?>)
    var tapOnStarObservable: Observable<Void> { get }
    func setIsStarred(newValue: Bool)
}

extension CoinView where Self: SubscriptionOwner {
    func configure(coin: CurrencyModel, imageSource: CoinImageSource) {
        configure(name: coin.currencyName, symbol: coin.currencySymbol, isStarred: coin.isFavorite, imageObservable: imageSource.getImage(for: coin))

        let disposeBag = DisposeBag()
        tapOnStarObservable.subscribe(onNext: { () in
            let isStarred = !coin.isFavorite
            coin.managedObjectContext?.performChanges {
                coin.isFavorite = isStarred
            }

            self.setIsStarred(newValue: isStarred)
        }).disposed(by: disposeBag)

        addSubscription(withName: "tapOnStarSubscription", disposeBag)
    }
}
