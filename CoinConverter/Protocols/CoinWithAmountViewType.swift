//
//  CurrencyAndAmountView.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

protocol CoinWithAmountViewType {
    func setAmount(_ amount: String)
    func setIsLoading(_ value: Bool)
    func setCoinName(_ value: String)
    func setCoinSymbol(_ value: String)
    func setCoinImage(_ image: UIImage?)
    func setHighlightedCoin(_ value: Bool)
    func setHighlightedAmount(_ value: Bool)
}
