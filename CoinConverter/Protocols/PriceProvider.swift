//
//  PriceProvider.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation
import RxSwift

enum PriceProviderResult {
    case price(Double)
    case notSupportedConversion
}

protocol PriceProvider {
    func getPrice(for currency: CurrencyType, baseCurrency: CurrencyType) -> Observable<PriceProviderResult>
}
