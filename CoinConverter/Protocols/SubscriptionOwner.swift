//
//  SubscriptionOwner.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

protocol SubscriptionOwner {
    func addSubscription(withName name: String, _ bag: DisposeBag)
    func addSubscription(withName name: String, _ observers: [NSObjectProtocol])
    func removeSubscription(_ name: String)
    func clearSubscriptions()
}

protocol DisposeBagDictionaryOwner: class {
    var namedDisposeBags: [String: DisposeBag] { get set }
}

extension SubscriptionOwner where Self: DisposeBagDictionaryOwner {
    func addSubscription(withName name: String, _ bag: DisposeBag) {
        namedDisposeBags[name] = bag
    }

    func addSubscription(withName name: String, _ observers: [NSObjectProtocol]) {
        let disposeBag = DisposeBag()
        disposeBag.insert(ObserversDisposable(observers: observers))
        addSubscription(withName: name, disposeBag)
    }

    func removeSubscription(_ name: String) {
        namedDisposeBags[name] = nil
    }

    func clearSubscriptions() {
        namedDisposeBags.removeAll()
    }
}

fileprivate class ObserversDisposable: Disposable {
    private var observers: [NSObjectProtocol]?

    init(observers: [NSObjectProtocol]) {
        self.observers = observers
    }

    func dispose() {
        observers = nil
    }
}
