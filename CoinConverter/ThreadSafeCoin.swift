//
//  ThreadSafeCoin.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 04.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

struct ThreadSafeCoin: CurrencyType, Codable {
    var currencyID: Int?
    var currencyName: String
    var currencySymbol: String

    init(currency: CurrencyType) {
        currencyID = currency.currencyID
        currencyName = currency.currencyName
        currencySymbol = currency.currencySymbol
    }

    init(id: Int?, name: String, symbol: String) {
        currencyID = id
        currencyName = name
        currencySymbol = symbol
    }
}
