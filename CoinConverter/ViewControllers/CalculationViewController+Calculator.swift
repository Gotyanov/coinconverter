//
//  CalculationViewController+Calculator.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 04.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

extension CalculationViewController: Calculator {

    func setSourceCurrency(sourceCurrencyIsA: Bool) {
        self.currencyAIsSource = sourceCurrencyIsA
        keyboardView.switchFieldButtonHasUpDirection = !sourceCurrencyIsA
        sourceCurrencyView.setHighlightedAmount(true)
        targetCurrencyView.setHighlightedAmount(false)
    }

    var sourceCurrency: CurrencyType {
        if currencyAIsSource {
            return sourceAndTargetCurrencySetting.sourceCurrency
        }

        return sourceAndTargetCurrencySetting.targetCurrency
    }

    var targetCurrency: CurrencyType {
        if currencyAIsSource {
            return sourceAndTargetCurrencySetting.targetCurrency
        }

        return sourceAndTargetCurrencySetting.sourceCurrency
    }

    var sourceCurrencyView: CoinWithAmountViewType {
        if currencyAIsSource {
            return coinWithAmountViewA
        }

        return coinWithAmountViewB
    }

    var targetCurrencyView: CoinWithAmountViewType {
        if currencyAIsSource {
            return coinWithAmountViewB
        }

        return coinWithAmountViewA
    }

    func updateSource(formattedAmount: String) {
        sourceString = formattedAmount
        sourceCurrencyView.setAmount(formattedAmount)
    }

    func updateTarget(_ result: CalculatorResult) {
        targetCurrencyView.setIsLoading(false)
        switch result {
        case .formattedAmount(let formattedAmount):
            targetString = formattedAmount
            targetCurrencyView.setAmount(formattedAmount)
        case .error(_):
            targetString = "0"
            targetCurrencyView.setAmount("unavailable")
        case .inProgress:
            targetString = "0"
            targetCurrencyView.setIsLoading(true)
            targetCurrencyView.setAmount("")
        case .notSupportedConversion:
            targetString = "0"
            targetCurrencyView.setAmount("not supported")
        }
    }
}
