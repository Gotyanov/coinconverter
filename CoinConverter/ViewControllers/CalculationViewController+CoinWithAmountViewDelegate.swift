//
//  CalculationViewController+CoinWithAmountViewDelegate.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 04.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

extension CalculationViewController: CoinWithAmountViewDelegate {

    func setCurrencyUsedForSelection(isViewA: Bool) {
        currencyAIsUsedForCoinSelection = isViewA
    }

    func coinWithAmountViewWasTapOnCoin(_ view: CoinWithAmountView) {
        let isViewA = view == coinWithAmountViewA
        let otherView = isViewA ? coinWithAmountViewB! : coinWithAmountViewA!
        let prevCurrencyAIsUsedForCoinSelection = currencyAIsUsedForCoinSelection
        setCurrencyUsedForSelection(isViewA: isViewA)

        if currentState == .collapsed {
            animateTransition()
            view.setHighlightedCoin(true)
            otherView.setHighlightedCoin(false)
        } else {
            if prevCurrencyAIsUsedForCoinSelection == currencyAIsUsedForCoinSelection {
                animateTransition()
                view.setHighlightedCoin(false)
            } else {
                view.setHighlightedCoin(true)
                otherView.setHighlightedCoin(false)
            }
        }
    }

    func coinWithAmountViewWasTapOnAmount(_ view: CoinWithAmountView) {
        let isViewA = view == coinWithAmountViewA
        setSourceCurrency(sourceCurrencyIsA: isViewA)
    }
}
