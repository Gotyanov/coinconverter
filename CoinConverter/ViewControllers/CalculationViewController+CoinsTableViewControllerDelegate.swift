//
//  CalculationViewController+CoinsTableViewControllerDelegate.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 04.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

extension CalculationViewController: CoinsTableViewControllerDelegate {
    func coinsTableViewController(_ viewController: CoinsTableViewController, didSelect currency: CurrencyType) {
        if currencyAIsUsedForCoinSelection {
            sourceAndTargetCurrencySetting.sourceCurrency = currency
        } else {
            sourceAndTargetCurrencySetting.targetCurrency = currency
        }

        updateCoinViews()

        inputAmount(sourceString)
    }
}
