//
//  CalculationViewController+KeyboardViewDelegate.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 04.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

extension CalculationViewController: KeyboardViewDelegate {

    var sourceString: String {
        get {
            return currencyAIsSource ? amountA : amountB
        }
        set {
            if currencyAIsSource {
                amountA = newValue
            } else {
                amountB = newValue
            }
        }
    }

    var targetString: String {
        get {
            return currencyAIsSource ? amountB : amountA
        }
        set {
            if currencyAIsSource {
                amountB = newValue
            } else {
                amountA = newValue
            }
        }
    }

    func keyboardView(_ keyboardView: KeyboardView, numericButtonWasClicked: Int) {
        inputAmount(sourceString + numericButtonWasClicked.description)
    }

    func keyboardView(_ keyboardView: KeyboardView, button000WasClicked: Void) {
        inputAmount(sourceString + "000")
    }

    func keyboardView(_ keyboardView: KeyboardView, separatorButtonWasClicked: Void) {
        inputAmount(sourceString + (Locale.current.decimalSeparator ?? "."))
    }

    func keyboardView(_ keyboardView: KeyboardView, deleteButtonWasClicked: Void) {
        if sourceString.count <= 1 {
            inputAmount("0")
        } else {
            inputAmount(String(sourceString.prefix(sourceString.count - 1)))
        }
    }

    func keyboardView(_ keyboardView: KeyboardView, clearButtonWasClicked: Void) {
        inputAmount("0")
    }

    func keyboardView(_ keyboardView: KeyboardView, switchFieldButtonWasClicked: Void) {
        setSourceCurrency(sourceCurrencyIsA: !currencyAIsSource)
    }

    func keyboardView(_ keyboardView: KeyboardView, selectCoinButtonWasClicked: Void) {
        animateTransition()
    }
}
