//
//  CalculationViewController.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import CoreData
import RxSwift

final class CalculationViewController: UIViewController {

    let transitionDuration: TimeInterval = 0.4

    @IBOutlet var currencyValuesPairContainer: UIView!
    @IBOutlet var currencyValuesPairStackViewContainer: UIStackView!
    @IBOutlet var bottomPanel: UIView!
    @IBOutlet var keyboardView: KeyboardView!
    @IBOutlet var bottomPanelHeaderPanel: UIView!
    var coinWithAmountViewA: CoinWithAmountView!
    var coinWithAmountViewB: CoinWithAmountView!
    var amountA = "0"
    var amountB = "0"

    var namedDisposeBags: [String: DisposeBag] = [:]

    @IBOutlet var constraintsForCollapsedState: [NSLayoutConstraint] = []
    @IBOutlet var constraintsForExpandedState: [NSLayoutConstraint] = []

    var runningAnimators: [(animator: UIViewPropertyAnimator, source: TransitionBehavior)] = []
    var currentState: ExpandableViewState = .collapsed
    var destinationState: ExpandableViewState?
    var transitionBehaviors: [TransitionBehavior] = []
    var panVelocity: CGFloat = 0

    var toggleConstraintsBehavior: ToggleConstraintsBehavior?

    var coinsTableViewController: CoinsTableViewController?

    private var _priceProvider: PriceProvider!
    var priceProvider: PriceProvider {
        get { return _priceProvider }
        set { _priceProvider = newValue }
    }


    var viewContext: NSManagedObjectContext? {
        didSet {
            updateChildControllersDependencies()
        }
    }

    var imageSource: CoinImageSource? {
        didSet {
            updateChildControllersDependencies()
            updateCoinViews()
        }
    }

    var sourceAndTargetCurrencySetting: SourceAndTargetCurrencySetting!
    var currencyAIsSource: Bool = true
    var currencyAIsUsedForCoinSelection = true

    override func viewDidLoad() {
        super.viewDidLoad()
        loadCoinWithAmountViews()
        keyboardView.delegate = self
        setupTransitionBehaviors()
        addCornerRadiusToBottomPanel()
        let coinsTableViewController = storyboard!.instantiateViewController(withIdentifier: "CoinsTableViewController") as! CoinsTableViewController
        coinsTableViewController.delegate = self
        self.coinsTableViewController = coinsTableViewController
        updateChildControllersDependencies()

        DispatchQueue.main.async {
            if !coinsTableViewController.isViewLoaded {
                self.placeCoinsTableView()
            }
        }

        setSourceCurrency(sourceCurrencyIsA: true)
        updateCoinViews()
    }

    var isFirstLayout = true

    override func viewDidLayoutSubviews() {
        if isFirstLayout {
            isFirstLayout = false
            toggleConstraintsBehavior?.toggleConstraints(for: currentState)
        }

        super.viewDidLayoutSubviews()
    }

    func willAnimateTransition() {
        guard let coinsTableViewController = coinsTableViewController else {
            fatalError("coinsTableViewController should not be nill")
        }

        if currentState == .collapsed {
            coinsTableViewController.showKeyboardIfNeeded()
        } else {
            coinsTableViewController.hideKeyboardIfNeeded()
        }
    }

    private func addCornerRadiusToBottomPanel() {
        bottomPanel.layer.cornerRadius = 12
        bottomPanel.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    private func updateChildControllersDependencies() {
        coinsTableViewController?.viewContext = viewContext
        coinsTableViewController?.imageSource = imageSource
    }

    private func placeCoinsTableView() {
        guard let coinsTableViewController = coinsTableViewController else {
            fatalError("coinsTableViewController should be not nil")
        }

        let view = coinsTableViewController.view!
        view.translatesAutoresizingMaskIntoConstraints = false
        coinsTableViewController.setupBackgroundColor(color: bottomPanel.backgroundColor ?? UIColor.white)
        addChildViewController(coinsTableViewController)
        bottomPanel.addSubview(view)
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: bottomPanelHeaderPanel.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: bottomPanel.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: bottomPanel.trailingAnchor),
            view.bottomAnchor.constraint(equalTo: bottomPanel.bottomAnchor)
            ])

        coinsTableViewController.didMove(toParentViewController: self)
    }
}

extension CalculationViewController: ExpandableViewController, TransitionBehaviorDelegate {
    var collapsedValue: CGFloat {
        return view.bounds.height
    }

    var expandedValue: CGFloat {
        return view.safeAreaInsets.top + currencyValuesPairContainer.bounds.height
    }

    var currentValue: CGFloat {
        let layer = bottomPanel.layer.presentation() ?? bottomPanel.layer
        let layerOrigin = layer.convert(layer.bounds.origin, to: view.layer)
        return layerOrigin.y
    }

    func getDuration(forMovingTo state: ExpandableViewState) -> TimeInterval {
        return transitionDuration
    }

    func setupTransitionBehaviors() {
        let toggleConstraintsBehavior = ToggleConstraintsBehavior(
            constraintsForCollapsedState: constraintsForCollapsedState,
            constraintsForExpandedState: constraintsForExpandedState,
            viewToLayout: view)

        self.toggleConstraintsBehavior = toggleConstraintsBehavior

        addTransitionBehavior(toggleConstraintsBehavior)

        let tapGestureBehavior = TapGestureBehavior(viewToTap: bottomPanelHeaderPanel)
        addTransitionBehavior(tapGestureBehavior)

        let panGestureBehavior = PanGestureBehavior(viewToPan: bottomPanelHeaderPanel)
        addTransitionBehavior(panGestureBehavior)

        let blockTransitionBehavior = BlockTransitionBehavior { state in
            self.updateHighlightingCoinViews(currentState: state)
        }

        addTransitionBehavior(blockTransitionBehavior)
    }
}

extension CalculationViewController: SubscriptionOwner, DisposeBagDictionaryOwner { }

extension CalculationViewController {
    private func loadCoinWithAmountViews() {
        func createCoinWithAmountView() -> CoinWithAmountView {
            let view = Bundle.main.loadNibNamed("CoinWithAmountView", owner: nil, options: nil)![0] as! CoinWithAmountView
            return view
        }

        coinWithAmountViewA = createCoinWithAmountView()
        coinWithAmountViewB = createCoinWithAmountView()
        coinWithAmountViewA.delegate = self
        coinWithAmountViewB.delegate = self
        coinWithAmountViewA.setAmount(amountA)
        coinWithAmountViewB.setAmount(amountB)
        currencyValuesPairStackViewContainer.addArrangedSubview(coinWithAmountViewA)
        currencyValuesPairStackViewContainer.addArrangedSubview(coinWithAmountViewB)
    }

    private func updateCoinView(_ view: CoinWithAmountViewType, coin: CurrencyType, _ viewID: String) {
        view.setCoinName(coin.currencyName)
        view.setCoinSymbol(coin.currencySymbol)
        view.setCoinImage(nil)

        guard let imageSource = self.imageSource else { return }
        let disposeBag = DisposeBag()
        imageSource.getImage(for: coin).subscribe(onNext: { image in
            DispatchQueue.main.async {
                view.setCoinImage(image)
            }
        }).disposed(by: disposeBag)

        addSubscription(withName: "updateImageFor" + viewID, disposeBag)
    }

    func updateCoinViews() {
        guard let viewA = coinWithAmountViewA, let viewB = coinWithAmountViewB else { return }
        updateCoinView(viewA, coin: sourceAndTargetCurrencySetting.sourceCurrency, "A")
        updateCoinView(viewB, coin: sourceAndTargetCurrencySetting.targetCurrency, "B")
    }

    private func updateHighlightingCoinViews(currentState: ExpandableViewState) {
        guard let viewA = coinWithAmountViewA, let viewB = coinWithAmountViewB else { return }
        if currentState == .collapsed {
            viewA.setHighlightedCoin(false)
            viewB.setHighlightedCoin(false)

        } else {
            let view = currencyAIsUsedForCoinSelection ? viewA : viewB
            view.setHighlightedCoin(true)
        }
    }
}
