//
//  CoinsTableViewController.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import CoreData
import RxSwift

protocol CoinsTableViewControllerDelegate: class {
    func coinsTableViewController(_ viewController: CoinsTableViewController, didSelect currency: CurrencyType)
}

class CoinsTableViewController: UIViewController {
    weak var delegate: CoinsTableViewControllerDelegate?

    private let CellIdentifier = "CoinTableViewCell"

    enum Filter: Int {
        case all = 0
        case crypto
        case fiat
        case starred

        var description: String {
            switch self {
            case .all: return "All"
            case .crypto: return "Crypto"
            case .fiat: return "Fiat"
            case .starred: return "Starred"
            }
        }

        static var allItems: [Filter] {
            return [.all, .crypto, .fiat, .starred]
        }
    }

    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!

    var namedDisposeBags: [String: DisposeBag] = [:]
    var imageSource: CoinImageSource?

    var searchQuerySubject = BehaviorSubject(value: "")
    var filterSubject = BehaviorSubject(value: Filter.all)

    var viewContext: NSManagedObjectContext? {
        didSet {
            if let context = viewContext {
                update(viewContext: context)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupScopeBar()

        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self

        adjustContentInsets(for: tableView)

        let disposeBag = DisposeBag()

        let searchQueryObservable = searchQuerySubject.throttledUserInput
        let filterObservable = filterSubject

        let searchWithFiltersObservable = Observable.combineLatest(searchQueryObservable, filterObservable, resultSelector: { ($0, $1) })
        searchWithFiltersObservable.observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (query, filter) in
                let compoundPredicate = NSCompoundPredicate(
                    andPredicateWithSubpredicates: [
                        CurrencyModel.defaultPredicate,
                        CurrencyModel.searchPredicate(query: query),
                        filter.predicateForCurrencyModel])

                self?.refresh(using: compoundPredicate)
            }).disposed(by: disposeBag)

        addSubscription(withName: "searchSubscription", disposeBag)
    }

    private func setupScopeBar() {
        searchBar.showsScopeBar = true
        searchBar.scopeButtonTitles = Filter.allItems.map { $0.description }
        searchBar.selectedScopeButtonIndex = try! filterSubject.value().rawValue
    }

    private func refresh(using predicate: NSPredicate) {
        guard let fetchedResultsController = fetchedResultsController, let tableView = tableView else { return }
        fetchedResultsController.fetchRequest.predicate = predicate
        try! fetchedResultsController.performFetch()
        tableView.reloadData()
    }

    var fetchedResultsController: NSFetchedResultsController<CurrencyModel>?

    func update(viewContext: NSManagedObjectContext) {
        let request = CurrencyModel.sortedFetchRequest
        request.fetchBatchSize = 25

        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: viewContext, sectionNameKeyPath: nil, cacheName: nil)

        self.fetchedResultsController = fetchedResultsController
        refresh(using: request.predicate ?? NSPredicate(value: true))
    }
}

extension CoinsTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController?.sections?[section].numberOfObjects ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath)
        guard let frc = fetchedResultsController else {
            return cell
        }

        guard let coinCell = cell as? CoinTableViewCell else {
            fatalError("Wrong type of coinCell. Should be \(CoinTableViewCell.self)")
        }

        let currency = frc.object(at: indexPath)

        if let imageSource = imageSource {
            coinCell.configure(coin: currency, imageSource: imageSource)
        }

        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 0
    }
}

extension CoinsTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let object = fetchedResultsController!.object(at: indexPath)
        delegate?.coinsTableViewController(self, didSelect: object)
    }
}

extension CoinsTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchQuerySubject.onNext(searchText.trimmingCharacters(in: .whitespaces))
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        guard let filter = Filter(rawValue: selectedScope) else {
            fatalError("Unknown scope: \(selectedScope)")
        }

        filterSubject.onNext(filter)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension CoinsTableViewController.Filter {
    var predicateForCurrencyModel: NSPredicate {
        switch self {
        case .all: return NSPredicate(value: true)
        case .crypto: return CurrencyModel.cryptoCurrencyPredicate()
        case .fiat: return CurrencyModel.fiatCurrencyPredicate()
        case .starred: return CurrencyModel.starredCurrencyPredicate()
        }
    }
}

extension CoinsTableViewController: SubscriptionOwner, DisposeBagDictionaryOwner { }

extension CoinsTableViewController {
    func setupBackgroundColor(color: UIColor) {
        view.backgroundColor = color
        searchBar.barTintColor = color
        tableView.backgroundColor = color
    }
}

extension CoinsTableViewController {
    func hideKeyboardIfNeeded() {
        searchBar.resignFirstResponder()
    }

    func showKeyboardIfNeeded() {
        guard let currentFilter = try? filterSubject.value() else {
            return
        }

        if currentFilter == .all || currentFilter == .crypto {
            searchBar.becomeFirstResponder()
        }
    }
}
