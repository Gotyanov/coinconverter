//
//  CoinTableViewCell.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

final class CoinTableViewCell: UITableViewCell {

    private let starredColor = #colorLiteral(red: 0.9607843137, green: 0.7019607843, blue: 0, alpha: 1)
    private let notStarredColor = #colorLiteral(red: 0.8328605294, green: 0.833000958, blue: 0.832842052, alpha: 1)

    var namedDisposeBags: [String: DisposeBag] = [:]

    @IBOutlet var coinImageView: UIImageView!
    @IBOutlet var coinNameLabel: UILabel!
    @IBOutlet var coinSymbolLabel: UILabel!
    @IBOutlet var coinStarButton: UIButton!

    let tapOnStarSubject = PublishSubject<Void>()

    @IBAction func tapOnStar() {
        tapOnStarSubject.onNext(())
    }
}

extension CoinTableViewCell: SubscriptionOwner, DisposeBagDictionaryOwner { }

extension CoinTableViewCell: CoinView {
    func setIsStarred(newValue: Bool) {
        coinStarButton.tintColor = newValue ? starredColor : notStarredColor
    }

    var tapOnStarObservable: Observable<Void> {
        return tapOnStarSubject
    }

    func configure(name: String, symbol: String, isStarred: Bool, imageObservable: Observable<UIImage?>) {
        coinImageView.image = nil
        coinNameLabel.text = name
        coinSymbolLabel.text = symbol
        setIsStarred(newValue: isStarred)

        var cancelSetImage = false

        let disposeBag = DisposeBag()
        imageObservable.observeOn(MainScheduler.asyncInstance)
            .subscribe(
                onNext: { [weak coinImageView] image in
                    guard !cancelSetImage, let coinImageView = coinImageView else { return }
                    
                    coinImageView.image = image
                },
                onDisposed: { cancelSetImage = true })
            .disposed(by: disposeBag)

        addSubscription(withName: "imageObservable", disposeBag)
    }
}
