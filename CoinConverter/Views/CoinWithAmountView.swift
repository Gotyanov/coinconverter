//
//  CoinWithAmountView.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 03.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

protocol CoinWithAmountViewDelegate: class {
    func coinWithAmountViewWasTapOnCoin(_ view: CoinWithAmountView)
    func coinWithAmountViewWasTapOnAmount(_ view: CoinWithAmountView)
}

final class CoinWithAmountView: UIView {
    weak var delegate: CoinWithAmountViewDelegate?

    let defaultCoinBackground = UIColor.white
    let highlightedCoinBackground = #colorLiteral(red: 1, green: 0.9501952529, blue: 0.6817888021, alpha: 1)

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var symbolLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var backgroundViewForCoin: UIView!

    @IBAction func handleTapOnCoinView() {
        delegate?.coinWithAmountViewWasTapOnCoin(self)
    }

    @IBAction func handleTapOnAmountView() {
        delegate?.coinWithAmountViewWasTapOnAmount(self)
    }
}

extension CoinWithAmountView: CoinWithAmountViewType {
    func setAmount(_ amount: String) {
        amountLabel.text = amount
    }

    func setIsLoading(_ value: Bool) {
        if value {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }

    func setCoinName(_ value: String) {
        nameLabel.text = value
    }

    func setCoinSymbol(_ value: String) {
        symbolLabel.text = value
    }

    func setCoinImage(_ image: UIImage?) {
        imageView.image = image
    }

    func setHighlightedCoin(_ value: Bool) {
        let color = value ? highlightedCoinBackground : defaultCoinBackground
        backgroundViewForCoin.backgroundColor = color
    }

    func setHighlightedAmount(_ value: Bool) {
        if value {
            amountLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        } else {
            amountLabel.font = UIFont.systemFont(ofSize: 18, weight: .light)
        }
    }
}
