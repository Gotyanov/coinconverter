//
//  KeyboardView.swift
//  CoinConverter
//
//  Created by Aleksey Gotyanov on 01.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import AVKit
import UIKit
import RxSwift

fileprivate let primaryColor1 = UIColor.white
fileprivate let primaryColor2 = #colorLiteral(red: 1, green: 0.9501952529, blue: 0.6817888021, alpha: 1)

protocol KeyboardViewDelegate: class {
    func keyboardView(_ keyboardView: KeyboardView, numericButtonWasClicked: Int)
    func keyboardView(_ keyboardView: KeyboardView, button000WasClicked: Void)
    func keyboardView(_ keyboardView: KeyboardView, separatorButtonWasClicked: Void)
    func keyboardView(_ keyboardView: KeyboardView, deleteButtonWasClicked: Void)
    func keyboardView(_ keyboardView: KeyboardView, clearButtonWasClicked: Void)
    func keyboardView(_ keyboardView: KeyboardView, switchFieldButtonWasClicked: Void)
    func keyboardView(_ keyboardView: KeyboardView, selectCoinButtonWasClicked: Void)
}

@IBDesignable
final class KeyboardView: UIView {

    weak var delegate: KeyboardViewDelegate?

    override func willMove(toSuperview newSuperview: UIView?) {
        configure()
    }

    private var jumpToNextFieldButton: KeyboardButton?
    private func updateJumpToNextFieldButtonDirection() {
        switch switchFieldButtonHasUpDirection {
        case true: jumpToNextFieldButton?.setTitle("↑", for: .normal)
        case false: jumpToNextFieldButton?.setTitle("↓", for: .normal)
        }
    }

    var switchFieldButtonHasUpDirection: Bool = false {
        didSet {
            updateJumpToNextFieldButtonDirection()
        }
    }

    private func configure() {
        var numericButtons: [KeyboardButton] = []
        for i in 0...9 {
            let keyboardButton = KeyboardButton()
            keyboardButton.tag = i
            keyboardButton.setTitle(i.description, for: .normal)
            keyboardButton.addTarget(self, action: #selector(numericButtonWasClicked(_:)), for: .touchUpInside)
            numericButtons.append(keyboardButton)
        }

        let separatorButton = KeyboardButton()
        let separator = Locale.current.decimalSeparator ?? "."
        separatorButton.setTitle(separator, for: .normal)
        separatorButton.addTarget(self, action: #selector(separatorButtonWasClicked(_:)), for: .touchUpInside)

        let button000 = KeyboardButton()
        button000.setTitle("000", for: .normal)
        button000.addTarget(self, action: #selector(button000WasClicked(_:)), for: .touchUpInside)

        let deleteButton = KeyboardDeleteButton()
        deleteButton.addTarget(self, action: #selector(deleteButtonWasClicked(_:)), for: .touchUpInside)

        let clearAllButton = KeyboardClearButton()
        clearAllButton.addTarget(self, action: #selector(clearButtonWasClicked(_:)), for: .touchUpInside)

        let jumpToNextField = KeyboardDarkButton()
        jumpToNextField.setTitle("", for: .normal)
        jumpToNextField.addTarget(self, action: #selector(jumpToNextFieldButtonWasClicked(_:)), for: .touchUpInside)
        self.jumpToNextFieldButton = jumpToNextField
        updateJumpToNextFieldButtonDirection()

        let selectCoinButton = KeyboardDarkButton()
        selectCoinButton.setTitle("Select Coin", for: .normal)
        selectCoinButton.titleLabel?.numberOfLines = 0
        selectCoinButton.titleLabel?.font = UIFont.systemFont(ofSize: 24)
        selectCoinButton.titleLabel?.adjustsFontSizeToFitWidth = true
        selectCoinButton.titleLabel?.textAlignment = .center
        selectCoinButton.addTarget(self, action: #selector(selectCoinButtonWasClicked(_:)), for: .touchUpInside)

        let group1 = [numericButtons[1], numericButtons[4], numericButtons[7], separatorButton]
        let group2 = [numericButtons[2], numericButtons[5], numericButtons[8], numericButtons[0]]
        let group3 = [numericButtons[3], numericButtons[6], numericButtons[9], button000]
        let group4 = [deleteButton, clearAllButton, jumpToNextField, selectCoinButton]

        var verticalStacks: [UIStackView] = []
        for group in [group1, group2, group3, group4] {
            let stack = UIStackView(arrangedSubviews: group)
            stack.axis = .vertical
            stack.alignment = .fill
            stack.distribution = .fillEqually
            stack.spacing = 1

            verticalStacks.append(stack)
        }

        let rootStackView = UIStackView(arrangedSubviews: verticalStacks)
        rootStackView.axis = .horizontal
        rootStackView.alignment = .fill
        rootStackView.distribution = .fillEqually
        rootStackView.spacing = 1

        rootStackView.frame = bounds
        rootStackView.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        addSubview(rootStackView)
        backgroundColor = UIColor.black.withAlphaComponent(0.05)
    }

    @objc private func numericButtonWasClicked(_ button: KeyboardButton) {
        delegate?.keyboardView(self, numericButtonWasClicked: button.tag)
    }

    @objc private func button000WasClicked(_ button: KeyboardButton) {
        delegate?.keyboardView(self, button000WasClicked: ())
    }

    @objc private func separatorButtonWasClicked(_ button: KeyboardButton) {
        delegate?.keyboardView(self, separatorButtonWasClicked: ())
    }

    @objc private func deleteButtonWasClicked(_ button: KeyboardButton) {
        delegate?.keyboardView(self, deleteButtonWasClicked: ())
    }

    @objc private func clearButtonWasClicked(_ button: KeyboardButton) {
        delegate?.keyboardView(self, clearButtonWasClicked: ())
    }

    @objc private func jumpToNextFieldButtonWasClicked(_ button: KeyboardButton) {
        delegate?.keyboardView(self, switchFieldButtonWasClicked: ())
    }

    @objc private func selectCoinButtonWasClicked(_ button: KeyboardButton) {
        delegate?.keyboardView(self, selectCoinButtonWasClicked: ())
    }
}

class KeyboardButton: UIButton {

    internal private(set) var normalBackgroundColor: UIColor = primaryColor1
    internal private(set) var highlightedBackgroundColor = primaryColor2

    var isButtonHighlighted: Bool = false {
        didSet {
            guard isButtonHighlighted != oldValue else { return }
            isButtonHighlightedChanged()
        }
    }

    func isButtonHighlightedChanged() {
        backgroundColor = isButtonHighlighted ? highlightedBackgroundColor : normalBackgroundColor
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    var observation: NSKeyValueObservation?

    func setup() {
        let font = UIFont.systemFont(ofSize: 28, weight: .regular)

        titleLabel?.font = font
        setTitleColor(.black, for: .normal)
        backgroundColor = normalBackgroundColor

        addTarget(self, action: #selector(onTouchDown), for: UIControlEvents.touchDown)
        addTarget(self, action: #selector(onTouchUp), for: [UIControlEvents.touchCancel, UIControlEvents.touchUpInside, UIControlEvents.touchUpOutside])
    }

    @objc func onTouchDown() {
        playClick()
        isButtonHighlighted = true
    }

    @objc func onTouchUp() {
        isButtonHighlighted = false
    }

    internal func playClick() {
        AudioServicesPlaySystemSound(SystemSoundID(1123))
    }

    override func pressesBegan(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        super.pressesBegan(presses, with: event)
    }
}

class KeyboardDarkButton: KeyboardButton {
    override var normalBackgroundColor: UIColor {
        return primaryColor2
    }

    override var highlightedBackgroundColor: UIColor {
        return primaryColor1
    }
}

final class KeyboardDeleteButton: KeyboardDarkButton {
    override func setup() {
        super.setup()
        self.setImage(UIImage(named: "Delete"), for: .normal)
        self.setImage(UIImage(named: "HighlightDelete"), for: .highlighted)
    }

    override func playClick() {
        AudioServicesPlaySystemSound(SystemSoundID(1155))
    }
}

final class KeyboardClearButton: KeyboardDarkButton {
    override func setup() {
        super.setup()
        self.setImage(UIImage(named: "HighlightClear"), for: .normal)
    }

    override func playClick() {
        AudioServicesPlaySystemSound(SystemSoundID(1155))
    }
}
