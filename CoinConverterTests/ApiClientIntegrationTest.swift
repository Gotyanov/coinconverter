//
//  ApiClientIntegrationTest.swift
//  CoinConverterTests
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import XCTest
import RxSwift
import RxBlocking
import RxTest
@testable import CoinConverter

class ApiClientIntegrationTest: XCTestCase {

    var apiClient: CoinMarketCapClient!
    let blockingTimeout: TimeInterval = 60

    override func setUp() {
        super.setUp()
        apiClient = CoinMarketCapClient()
    }
    
    func testListingRequest() throws {
        let listing = try apiClient.getListings().toBlocking(timeout: blockingTimeout).single()
        XCTAssert(!listing.data.isEmpty, "listing data should not be empty")
        XCTAssertNil(listing.metadata.error, "error should be nil")
    }

    func testTikerRequest() throws {
        let targetCurrency = "EUR"
        let ticker = try apiClient.getTicker(for: 1, convertTo: targetCurrency).toBlocking(timeout: blockingTimeout).single()
        XCTAssertEqual("Bitcoin", ticker.data.name)
        XCTAssertEqual("BTC", ticker.data.symbol)
        XCTAssertNotNil(ticker.data.quotes[targetCurrency], "quote for \(targetCurrency) should not be nil")

        let quote = ticker.data.quotes[targetCurrency]!
        XCTAssert(quote.price > 0, "price should be greater than zero")
        XCTAssertNil(ticker.metadata.error, "error should be nil")
    }
}
