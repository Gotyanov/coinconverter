//
//  AssetsTests.swift
//  CoinConverterTests
//
//  Created by Aleksey Gotyanov on 31.05.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import XCTest
@testable import CoinConverter

class AssetsTests: XCTestCase {

    let validFiatCurrencyValues: [String] = [
        "AUD", "BRL", "CAD", "CHF", "CLP",
        "CNY", "CZK", "DKK", "EUR", "GBP",
        "HKD", "HUF", "IDR", "ILS", "INR",
        "JPY", "KRW", "MXN", "MYR", "NOK",
        "NZD", "PHP", "PKR", "PLN", "RUB",
        "SEK", "SGD", "THB", "TRY", "TWD",
        "ZAR"
    ]

    func testAllValidFiatCurrencyValuesShouldHaveImage() {
        var missedCurrencyImages: [String] = []
        for currency in validFiatCurrencyValues {
            let image = UIImage(named: currency)
            if image == nil {
                missedCurrencyImages.append(currency)
            }
        }

        XCTAssert(missedCurrencyImages.isEmpty,
                  "All fiat currency values should have icon. Icons are missed for: " + missedCurrencyImages.joined(separator: ", "))
    }

    func testCurrencyListShouldContainAllValidCurrencyValues() {
        guard let url = Bundle.main.url(forResource: "CurrencyList", withExtension: "plist"),
            let data = try? Data(contentsOf: url)
            else { XCTFail("Can not open CurrencyList.plist"); return }

        let decoder = PropertyListDecoder()
        let currencyInfos: [FiatCurrency] = try! decoder.decode([FiatCurrency].self, from: data)

        var missedCurrencyInfo: [String] = []
        for currency in validFiatCurrencyValues {
            let currencyInfoIsMissing = currencyInfos.first(where: { $0.code == currency }) == nil
            if currencyInfoIsMissing {
                missedCurrencyInfo.append(currency)
            }
        }

        XCTAssert(missedCurrencyInfo.isEmpty, "Fiat currency info are missed for: " + missedCurrencyInfo.joined(separator: ", "))
    }

    func testInstalledListingShouldBeAccessible() {
        let installedListing = InstalledListingFromCoinMarket().getCurrencyValues()
        XCTAssert(!installedListing.isEmpty)
    }
}
