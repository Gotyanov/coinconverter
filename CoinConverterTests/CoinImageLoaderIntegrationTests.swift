//
//  CoinImageLoaderIntegrationTests.swift
//  CoinConverterTests
//
//  Created by Aleksey Gotyanov on 02.06.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import XCTest
import RxSwift
import RxBlocking
@testable import CoinConverter

class CoinImageLoaderIntegrationTests: XCTestCase {

    var imageLoader: CoinImageLoader!
    let blockingTimeout: TimeInterval = 60

    override func setUp() {
        super.setUp()
        imageLoader = CoinImageLoader()
    }
    
    func testLoadingImageForBitcoin() throws {
        let image = try imageLoader.loadImage(for: 1).toBlocking(timeout: blockingTimeout).single()
        XCTAssertNotNil(image)
    }
    
}
